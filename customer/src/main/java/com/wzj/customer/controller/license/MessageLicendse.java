package com.wzj.customer.controller.license;


import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Component
@RabbitListener(queues = "process.queue")//监听的队列名称 TestDirectQueue
public class MessageLicendse {
    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println("DirectReceiver消费者收到消息  : " + testMessage.toString());
        System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date()).toString());
    }
}

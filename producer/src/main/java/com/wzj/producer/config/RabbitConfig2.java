package com.wzj.producer.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig2 {

    /**为了更贴合业务，参数名不使用DeadQueue之类的*/
    /**延迟队列名*/
    private static String DELAY_QUEUE = "delay.queue";
    /**延迟队列(死信队列)交换器名*/
    private static String DELAY_EXCHANGE = "delay.exchange";
    /**处理业务的队列(死信队列)*/
    private static String PROCESS_QUEUE = "process.queue";
    /**ttl(10秒)*/
    private static int DELAY_EXPIRATION = 10000;


    /**
     * 创建延迟队列
     * "x-dead-letter-exchange"参数定义死信队列交换机
     * "x-dead-letter-routing-key"定义死信队列中的消息重定向时的routing-key
     * "x-message-ttl"定义消息的过期时间
     */
    @Bean
    public Queue delayQueue(){
        return QueueBuilder.durable(DELAY_QUEUE)
                .withArgument("x-dead-letter-exchange", DELAY_EXCHANGE)
                .withArgument("x-dead-letter-routing-key", PROCESS_QUEUE)
                .withArgument("x-message-ttl", DELAY_EXPIRATION)
                .build();
    }

    /**创建用于业务的队列*/
    @Bean
    public Queue processQueue(){
        return QueueBuilder.durable(PROCESS_QUEUE)
                .build();
    }

    /**创建一个DirectExchange*/
    @Bean
    public DirectExchange delayExchange(){
        return new DirectExchange(DELAY_EXCHANGE);
    }

    /**绑定Exchange和queue，把消息重定向到业务queue*/
    @Bean
    Binding dlxBinding(){
        return BindingBuilder.bind(processQueue())
                .to(delayExchange())
                .with(PROCESS_QUEUE);
        //绑定，以PROCESS_QUEUE为routing key
    }




    //Direct交换机 起名：TestDirectExchange
    @Bean
    DirectExchange TestDirectExchange() {
        //  return new DirectExchange("TestDirectExchange",true,true);
        return new DirectExchange("TestDirectExchange",true,false);
    }

    //绑定  将队列和交换机绑定, 并设置用于匹配键：TestDirectRouting
    @Bean
    Binding bindingDirect() {
        return BindingBuilder.bind(delayQueue()).to(TestDirectExchange()).with("TestDirectRouting");
    }


}
